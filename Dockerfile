FROM debian:unstable-slim

#  install required dependencies
RUN apt-get update && apt-get install -y curl gpg xz-utils &&\
	curl https://dlang.org/install.sh | bash -s

RUN cd $(find ~/dlang -name "bin64" -type d) &&\
	find . -executable -type f | awk -F/ '{print $2}' | xargs -i ln -sf $PWD/{} /usr/bin/{} &&\
	cd ../lib64 &&\
	cp -R * /lib64/
